package net.sw4j.javadojo.roman;

/**
 * A class with a method to convert an integer into a Roman numeral.
 */
public class ToRoman {

    /**
     * Converts the given {@code value} in the range from {@code 1} to
     * {@code 3999} into a Roman numeral.
     *
     * @param value the {@code value} to convert.
     * @return the Roman numeral.
     */
    public String toRoman(int value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
